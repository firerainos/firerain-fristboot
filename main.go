package main

import (
	"github.com/therecipe/qt/widgets"
	"gitlab.com/firerainos/firerain-firstboot/ui"
	"gitlab.com/xiayesuifeng/go-i18n"
	"os"
)

func main() {
	app := widgets.NewQApplication(len(os.Args), os.Args)
	app.SetApplicationName("firerain-firstboot")
	app.SetApplicationVersion("0.1.0")

	i18n.LoadTranslator()

	mainwindow := ui.NewMainWindow()
	mainwindow.SetFixedSize(app.PrimaryScreen().Geometry().Size())
	mainwindow.ShowFullScreen()

	os.Exit(app.Exec())
}
