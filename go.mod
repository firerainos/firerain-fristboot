module gitlab.com/firerainos/firerain-firstboot

go 1.16

require (
	github.com/therecipe/qt v0.0.0-20200904063919-c0c124a5770d
	gitlab.com/xiayesuifeng/go-i18n v0.0.0-20210331070049-e1f6fbdb25c2
)
