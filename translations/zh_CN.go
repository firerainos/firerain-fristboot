package translations

import (
	"gitlab.com/xiayesuifeng/go-i18n"
)

func init() {
	i18n.AddTranslation(i18n.Translation{
		"Back":                              "返回",
		"Continue":                          "继续",
		"OK":                                "确定",
		"Start using":                       "开始使用",
		"Create user account":               "创建用户帐号",
		"Enter username and password":       "输入用户名和密码",
		"Username":                          "用户名",
		"Hostname":                          "主机名",
		"Password":                          "密码",
		"Confirm user password":             "确认用户密码",
		"Username can not be empty":         "用户名不能为空",
		"The hostname cannot be empty":      "主机名不能为空",
		"Password can not be blank":         "密码不能为空",
		"Confirm password can not be blank": "确认密码不能为空",
		"Confirm password does not match":   "确认密码不匹配",
		"User creation failed, please switch to tty2 to create manually after clicking Start:\n": "用户创建失败,请在点击开始使用后切换到tty2手动创建:\n",
		"The hostname setting failed, you can set it manually:\n":                                "主机名设置失败,可手动设置:\n",
		"Locale setting failed, you can set it manually:\n":                                      "Locale设置失败,可手动设置:\n",
		"The input method environment variable setting failed, you can set it manually:\n":       "输入法环境变量设置失败,可手动设置:\n",
		"Please wait...": "请稍等...",
		"Welcome":        "欢迎使用",
	}, "zh_CN")
}
