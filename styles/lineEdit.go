package styles

const LineEdit string = `QLineEdit {
			background: rgba(255,255,255,50);
            border: 1px solid transparent;
            border-radius: 4px;
            padding-left: 32px;
            outline: none;
            font-size: 12px;
            line-height: 1.25;
          }
          
          QLineEdit:focus {
            border: 1px solid #2ca7f8;
          }
`
