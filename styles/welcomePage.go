package styles

const LangListWidget string = `QListWidget {
	background: rgba(255,255,255,50);
    border: 1px solid transparent;
    border-radius: 4px;
}

QListWidget::item {
	padding: 5px;
    color:white;
	border:0;
}

QListWidget::item:hover {
    color:white;
	border-radius: 4px;
	background: rgba(255,255,255,50);
}

QListWidget::item:selected {
	border-radius: 4px;
	background: rgba(255,255,255,150);
    color:gray;
}

QScrollBar {width: 2px}`
