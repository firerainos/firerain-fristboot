package page

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/widgets"
	widgets2 "gitlab.com/firerainos/firerain-firstboot/ui/widgets"
	"gitlab.com/xiayesuifeng/go-i18n"
)

type AccountPage struct {
	*widgets.QFrame

	titleLabel,tipsLabel *widgets.QLabel

	Username, Hostname, Password, AgainPassword *widgets2.LineEdit
}

func NewAccountPage(parent widgets.QWidget_ITF, fo core.Qt__WindowType) *AccountPage {
	frame := widgets.NewQFrame(parent, fo)

	accountPage := &AccountPage{QFrame: frame}
	accountPage.init()
	accountPage.initConnect()

	return accountPage
}

func (page *AccountPage) init() {
	vboxLayout := widgets.NewQVBoxLayout2(page)

	logoLabel := widgets.NewQLabel(page, 0)

	logoLabel.SetPixmap(gui.NewQPixmap3(":/resources/logo.png", "", 0).
		Scaled2(150, 150, core.Qt__KeepAspectRatioByExpanding, 0))
	logoLabel.SetFixedSize2(150, 150)

	page.titleLabel = widgets.NewQLabel2(i18n.Tr("Create user account"), page, 0)

	page.tipsLabel = widgets.NewQLabel2(i18n.Tr("Enter username and password"), page, 0)
	page.tipsLabel.SetMinimumWidth(200)
	page.tipsLabel.SetAlignment(core.Qt__AlignCenter)

	page.Username = widgets2.NewLineEdit(":/resources/username.svg", page)
	page.Hostname = widgets2.NewLineEdit(":/resources/host.svg", page)
	page.Password = widgets2.NewLineEdit(":/resources/password.svg", page)
	page.AgainPassword = widgets2.NewLineEdit(":/resources/password.svg", page)

	regExp := core.NewQRegExp()
	regExp.SetPattern("^[a-z][-a-z0-9_]*$")

	page.Username.SetValidator(gui.NewQRegExpValidator2(regExp, page))

	page.Username.SetPlaceholderText(i18n.Tr("Username"))
	page.Hostname.SetPlaceholderText(i18n.Tr("Hostname"))
	page.Password.SetPlaceholderText(i18n.Tr("Password"))
	page.AgainPassword.SetPlaceholderText(i18n.Tr("Confirm user password"))

	page.Password.SetEchoMode(widgets.QLineEdit__Password)
	page.AgainPassword.SetEchoMode(widgets.QLineEdit__Password)

	vboxLayout.AddSpacing(100)
	vboxLayout.AddStretch(8)
	vboxLayout.AddWidget(logoLabel, 0, core.Qt__AlignHCenter)
	vboxLayout.AddSpacing(30)
	vboxLayout.AddWidget(page.titleLabel, 0, core.Qt__AlignHCenter)
	vboxLayout.AddWidget(page.tipsLabel, 0, core.Qt__AlignHCenter)
	vboxLayout.AddSpacing(100)
	vboxLayout.AddStretch(10)
	vboxLayout.AddWidget(page.Username, 0, core.Qt__AlignCenter)
	vboxLayout.AddWidget(page.Hostname, 0, core.Qt__AlignCenter)
	vboxLayout.AddWidget(page.Password, 0, core.Qt__AlignCenter)
	vboxLayout.AddWidget(page.AgainPassword, 0, core.Qt__AlignCenter)
	vboxLayout.AddStretch(1)

	page.SetLayout(vboxLayout)
}
func (page *AccountPage) initConnect() {
	page.Username.ConnectTextChanged(func(text string) {
		page.Hostname.SetText(text + "-PC")
	})
}

func (page *AccountPage) Check() bool {
	if page.Username.Text() == "" {
		page.SetTips(i18n.Tr("Confirm user password"))
	} else if page.Hostname.Text() == "" {
		page.SetTips(i18n.Tr("Username can not be empty"))
	} else if page.Password.Text() == "" {
		page.SetTips(i18n.Tr("The hostname cannot be empty"))
	} else if page.AgainPassword.Text() == "" {
		page.SetTips(i18n.Tr("Password can not be blank"))
	} else if page.Password.Text() != page.AgainPassword.Text() {
		page.SetTips(i18n.Tr("Confirm password can not be blank"))
	} else {
		return true
	}

	return false
}

func (page *AccountPage) SetTips(tips string) {
	page.tipsLabel.SetText(tips)
}

func (page *AccountPage) LanguageChange() {
	page.titleLabel.SetText(i18n.Tr("Create user account"))
	page.tipsLabel.SetText(i18n.Tr("Enter username and password"))

	page.Username.SetPlaceholderText(i18n.Tr("Username"))
	page.Hostname.SetPlaceholderText(i18n.Tr("Hostname"))
	page.Password.SetPlaceholderText(i18n.Tr("Password"))
	page.AgainPassword.SetPlaceholderText(i18n.Tr("Confirm user password"))
}