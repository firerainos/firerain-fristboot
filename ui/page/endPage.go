package page

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/xiayesuifeng/go-i18n"
)

type EndPage struct {
	*widgets.QFrame

	tipsLabel *widgets.QLabel
}

func NewEndPage(parent widgets.QWidget_ITF, fo core.Qt__WindowType) *EndPage {
	page := &EndPage{QFrame: widgets.NewQFrame(parent, fo)}

	page.init()

	return page
}

func (page *EndPage) init() {
	vboxLayout := widgets.NewQVBoxLayout2(page)
	vboxLayout.SetContentsMargins(0, 0, 0, 0)

	page.tipsLabel = widgets.NewQLabel2(i18n.Tr("Please wait..."), page, 0)
	page.tipsLabel.SetAlignment(core.Qt__AlignCenter)
	page.tipsLabel.SetMinimumHeight(200)

	vboxLayout.AddStretch(1)
	vboxLayout.AddWidget(page.tipsLabel, 0, core.Qt__AlignCenter)
	vboxLayout.AddStretch(1)

	page.SetLayout(vboxLayout)
}

func (page *EndPage) AddTips(tips string) {
	page.tipsLabel.SetText(page.tipsLabel.Text() + "\n" + tips)
}

func (page *EndPage) SetWelcomeTips() {
	if page.tipsLabel.Text() == i18n.Tr("Please wait...") {
		page.tipsLabel.SetText(i18n.Tr("Welcome"))
	} else {
		page.AddTips(i18n.Tr("Welcome"))
	}
}

func (page *EndPage) LanguageChange() {
	page.tipsLabel.SetText(i18n.Tr("Please wait..."))
}
