package page

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/firerainos/firerain-firstboot/styles"
	widgets2 "gitlab.com/firerainos/firerain-firstboot/ui/widgets"
	"gitlab.com/xiayesuifeng/go-i18n"
)

type WelcomePage struct {
	widgets.QFrame

	langListWidget *widgets.QListWidget

	_ func() `constructor:"init"`

	_ func() `signal:"languageChange"`
}

func (w *WelcomePage) init() {
	vboxLayout := widgets.NewQVBoxLayout2(w)
	vboxLayout.SetContentsMargins(0, 0, 0, 0)

	hellWidget := widgets2.NewHelloWidget(w, 0)

	w.langListWidget = widgets.NewQListWidget(w)
	w.langListWidget.AddItems([]string{"English", "简体中文", "繁體中文 (香港)"})
	w.langListWidget.SetStyleSheet(styles.LangListWidget)

	vboxLayout.AddStretch(5)
	vboxLayout.AddWidget(hellWidget, 0, core.Qt__AlignCenter)
	vboxLayout.AddStretch(5)
	vboxLayout.AddWidget(w.langListWidget, 0, core.Qt__AlignHCenter|core.Qt__AlignBottom)
	vboxLayout.AddStretch(1)

	w.SetLayout(vboxLayout)

	w.initConnect()

	w.langListWidget.SetCurrentRow(1)
}

func (w *WelcomePage) initConnect() {
	w.langListWidget.ConnectCurrentTextChanged(func(currentText string) {
		if currentText == "English" {
			i18n.SetLanguage("en_US")
		} else if currentText == "简体中文" {
			i18n.SetLanguage("zh_CN")
		} else if currentText == "繁體中文 (香港)" {
			i18n.SetLanguage("zh_HK")
		}

		w.LanguageChange()
	})
}
