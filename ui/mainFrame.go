package ui

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	core2 "gitlab.com/firerainos/firerain-firstboot/core"
	"gitlab.com/firerainos/firerain-firstboot/styles"
	_ "gitlab.com/firerainos/firerain-firstboot/translations"
	"gitlab.com/firerainos/firerain-firstboot/ui/page"
	"gitlab.com/xiayesuifeng/go-i18n"
	"log"
	"os"
)

type MainFrame struct {
	*widgets.QFrame

	welcomePage *page.WelcomePage
	accountPage *page.AccountPage
	endPage     *page.EndPage

	backButton, nextButton *widgets.QPushButton

	stackLayout *widgets.QStackedLayout
}

func NewMainFrame(parent widgets.QWidget_ITF, fo core.Qt__WindowType) *MainFrame {
	frame := &MainFrame{QFrame: widgets.NewQFrame(parent, fo)}

	frame.init()
	frame.initConnect()

	return frame
}

func (m *MainFrame) init() {
	vboxLayout := widgets.NewQVBoxLayout2(m)
	vboxLayout.SetContentsMargins(0, 0, 0, 0)

	m.stackLayout = widgets.NewQStackedLayout()

	m.welcomePage = page.NewWelcomePage(m, 0)
	m.accountPage = page.NewAccountPage(m, 0)
	m.endPage = page.NewEndPage(m, 0)

	m.backButton = widgets.NewQPushButton2(i18n.Tr("Back"), m)
	m.nextButton = widgets.NewQPushButton2(i18n.Tr("Continue"), m)

	m.backButton.SetMinimumWidth(60)
	m.nextButton.SetMinimumWidth(60)

	m.backButton.SetStyleSheet(styles.BackButton)
	m.nextButton.SetStyleSheet(styles.NextButton)

	m.backButton.SetVisible(false)

	hboxLayout := widgets.NewQHBoxLayout()
	hboxLayout.SetSpacing(40)

	hboxLayout.AddStretch(1)
	hboxLayout.AddWidget(m.backButton, 0, core.Qt__AlignHCenter)
	hboxLayout.AddWidget(m.nextButton, 0, core.Qt__AlignHCenter)
	hboxLayout.AddStretch(1)

	m.stackLayout.AddWidget(m.welcomePage)
	m.stackLayout.AddWidget(m.accountPage)
	m.stackLayout.AddWidget(m.endPage)

	vboxLayout.AddLayout(m.stackLayout, 1)
	vboxLayout.AddSpacing(50)
	vboxLayout.AddLayout(hboxLayout, 1)
	vboxLayout.AddSpacing(50)

	m.SetLayout(vboxLayout)

	m.SetObjectName("MainFrame")
	m.SetStyleSheet(styles.MainFrame)
}

func (m *MainFrame) initConnect() {
	m.welcomePage.ConnectLanguageChange(m.LanguageChange)
	m.welcomePage.ConnectLanguageChange(m.accountPage.LanguageChange)
	m.welcomePage.ConnectLanguageChange(m.endPage.LanguageChange)

	m.stackLayout.ConnectCurrentChanged(func(index int) {
		if index == 0 {
			m.backButton.SetVisible(false)
		} else if index == m.stackLayout.Count()-1 {
			m.nextButton.SetVisible(false)
			m.backButton.SetVisible(false)
		} else if index > 0 {
			m.backButton.SetVisible(true)
		}
	})

	m.backButton.ConnectClicked(func(checked bool) {
		if m.nextButton.Text() == i18n.Tr("OK") {
			m.nextButton.SetText(i18n.Tr("Continue"))
		}
		m.stackLayout.SetCurrentIndex(m.stackLayout.CurrentIndex() - 1)
	})

	m.nextButton.ConnectClicked(func(checked bool) {
		switch m.nextButton.Text() {
		case i18n.Tr("Continue"):
			m.nextButton.SetText(i18n.Tr("OK"))
		case i18n.Tr("OK"):
			if !m.accountPage.Check() {
				return
			}
			go m.configuration()
		case i18n.Tr("Start using"):
			os.Exit(0)
		}
		m.stackLayout.SetCurrentIndex(m.stackLayout.CurrentIndex() + 1)
	})
}

func (m *MainFrame) configuration() {
	username := m.accountPage.Username.Text()

	if err := core2.UserAdd(username, m.accountPage.Password.Text()); err != nil {
		m.endPage.AddTips(i18n.Tr("User creation failed, please switch to tty2 to create manually after clicking Start:\n"))
	}

	if err := core2.SetHomeName(m.accountPage.Hostname.Text()); err != nil {
		m.endPage.AddTips(i18n.Tr("The hostname setting failed, you can set it manually:\n"))
	}

	if err := core2.SetLocale(username); err != nil {
		m.endPage.AddTips(i18n.Tr("Locale setting failed, you can set it manually:\n") + err.Error())
	}

	if err := core2.SetIM(username); err != nil {
		m.endPage.AddTips(i18n.Tr("The input method environment variable setting failed, you can set it manually:\n") + err.Error())
	}

	if err := core2.SetWelcomeAutoStart(username); err != nil {
		log.Println("set welcome autostart fail: ", err)
	}

	core2.DisableService()

	m.endPage.SetWelcomeTips()

	m.nextButton.SetText(i18n.Tr("Start using"))
	m.nextButton.SetVisible(true)
}

func (m *MainFrame) LanguageChange() {
	m.backButton.SetText(i18n.Tr("Back"))
	m.nextButton.SetText(i18n.Tr("Continue"))
}
